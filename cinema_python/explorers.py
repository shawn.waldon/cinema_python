import cinema_store
import itertools
import json

class Explorer(object):
    """
    Middleman that connects an arbitrary producing codes to the CinemaStore.
    The purpose of this class is to run through the parameter sets, and tell a
    set of tracks (in order) to do something with the parameter values
    it cares about.
    """

    def __init__(self,
        cinema_store,
        parameters, #these are the things that this explorer is responsible for and their ranges
        tracks #the things we pass off values to in order to do the work
        ):

        self.__cinema_store = cinema_store
        self.parameters = parameters
        self.tracks = tracks

    @property
    def cinema_store(self):
        return self.__cinema_store

    def list_parameters(self):
        """
        parameters is an ordered list of parameters that the Explorer varies over
        """
        return self.parameters

    def prepare(self):
        """ Give tracks a chance to get ready for a run """
        if self.tracks:
            for e in self.tracks:
                res = e.prepare(self)

    def execute(self, desc):
        # Create the document/data product for this sample.
        doc = cinema_store.Document(desc)
        for e in self.tracks:
            e.execute(doc)
        self.insert(doc)

    def explore(self, fixedargs=None):
        """
        Explore the problem space to populate the store.
        Fixed arguments are the parameters that we want to hold constant in the exploration.
        Be careful to never hit spaces where parameter dependencies are not satisfied.
        """
        self.prepare()

        dependencies = self.cinema_store.parameter_associations
        #print "DEPS", dependencies

        #prepare to iterate through all the possibilities, in order
        param_names = self.list_parameters()
        params = []
        values = []
        dep_params = []
        for name in param_names:
            vals = self.cinema_store.get_parameter(name)['values']
            if fixedargs and name in fixedargs:
                continue
            params.append(name)
            values.append(vals)

        #the algorithm is to iterate through all combinations, and remove
        #the impossible ones. I use a set to avoid redundant combinations.
        #In order to use the set I serialize to make something hashable.
        #Then I insert into a list to preserve the (hopefully optimized) order.
        ok_descs = set()
        ordered_descs = []
        for element in itertools.product(*values):
            descriptor = dict(itertools.izip(params, element))

            if fixedargs != None:
                descriptor.update(fixedargs)

            ok_params = []
            ok_vals = []

            ok_desc = {}
            for param, value in descriptor.iteritems():
                if self.cinema_store.dependencies_satisfied(param, descriptor):
                    ok_desc.update({param:value})

            strval = json.dumps(ok_desc, sort_keys=True)
            if not strval in ok_descs:
                ok_descs.add(strval)
                ordered_descs.append(ok_desc)

        for descriptor in ordered_descs:
            self.execute(descriptor)

        self.finish()

    def finish(self):
        """ Give tracks a chance to clean up after a run """
        if self.tracks:
            for e in self.tracks:
                res = e.finish()

    def insert(self, doc):
        self.cinema_store.insert(doc)

class Track(object):
    """
    abstract interface for things that can produce data

    to use this:
    caller should set up some visualization
    then tie a particular set of parameters to an action with a track
    """

    def __init__(self):
        pass

    def prepare(self, explorer):
        """ subclasses get ready to run here """
        pass

    def finish(self):
        """ subclasses cleanup after running here """
        pass

    def execute(self, document):
        """ subclasses operate on parameters here"""
        pass

class LayerControl(object):
    """
    Prototype for something that Layer track can control
    """
    def __init__(self, name, showFunc, hideFunc):
        self.name = name
        self.callShow = showFunc  #todo, determine if function now and convert instead of try/except below
        self.callHide = hideFunc

class Layer(Track):
    """
    A track that connects a layer to the set of objects in the scene that it controls.
    """
    def __init__(self, layer, objectlist):
        super(Layer, self).__init__()
        self.parameter = layer
        # objlist is an array of class instances, they must have a name and
        #show and hide method, use LayerControl to make them.
        self.objectlist = objectlist

    def execute(self, doc):
        if not self.parameter in doc.descriptor:
            return
        o = doc.descriptor[self.parameter]
        for obj in self.objectlist:
            if obj.name == o:
                try:
                    obj.callShow() #method
                except TypeError:
                    obj.callShow(obj) #function
            else:
                try:
                    obj.callHide()
                except TypeError:
                    obj.callHide(obj)
