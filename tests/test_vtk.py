import unittest

from cinema_python import cinema_store
from cinema_python import explorers
from cinema_python import vtk_explorers
import vtk
from vtk.numpy_interface import dataset_adapter as dsa
import numpy

class TestImageFileStore(unittest.TestCase):
    def clean_up(self, cs, fname):
        import os
        for doc in cs.find():
            os.remove(cs._get_filename(doc))
        os.remove(fname)

    def test_basic(self):
        rw = vtk.vtkRenderWindow()
        rw.SetSize(500, 500)
        r = vtk.vtkRenderer()
        rw.AddRenderer(r)

        s = vtk.vtkRTAnalyticSource()
        s.SetWholeExtent(-25,25,-25,25,-25,25)

        cf = vtk.vtkContourFilter()
        cf.SetInputConnection(s.GetOutputPort())
        cf.SetInputArrayToProcess(0,0,0, "vtkDataObject::FIELD_ASSOCIATION_POINTS", "RTData")
        cf.SetNumberOfContours(1)
        cf.SetValue(0, 200)
        cf.ComputeScalarsOn()
        m = vtk.vtkPolyDataMapper()
        m.SetInputConnection(cf.GetOutputPort())
        a = vtk.vtkActor()
        a.SetMapper(m)
        r.AddActor(a)

        rw.Render()
        r.ResetCamera()

        # Create a new Cinema store
        cs = cinema_store.FileStore("./contour.json")
        cs.filename_pattern = "{phi}_{theta}_{contour}.png"

        # These are the parameters that we will have in the store
        cs.add_parameter("phi", cinema_store.make_parameter('phi', range(0,200,80)))
        cs.add_parameter("theta", cinema_store.make_parameter('theta', range(-180,200,80)))
        cs.add_parameter("contour", cinema_store.make_parameter('contour', [160, 200]))

        # These objects are responsible of change VTK parameters during exploration
        con = vtk_explorers.Contour('contour', cf, 'SetValue')
        cam = vtk_explorers.Camera([0,0,0], [0,1,0], 150.0, r.GetActiveCamera()) # phi,theta implied

        e = vtk_explorers.ImageExplorer(cs, ['contour', 'phi','theta'], [cam, con], rw)
        e.explore()

        # Now let's reproduce the first entry in the store

        # First set the camera to {'theta' : -180, 'phi' : 0}
        doc = cinema_store.Document({'theta' : -180, 'phi' : 0})
        cam.execute(doc)
        # Change the contour value
        cf.SetValue(0, 160)

        # Render & capture image
        rw.Render()

        w2i = vtk.vtkWindowToImageFilter()
        w2i.SetInput(rw)
        w2i.Update()

        image = w2i.GetOutput()

        # Convert image to a numpy array (flipped to have origin at upper left)
        npview = dsa.WrapDataObject(image)
        idata = npview.PointData[0]
        ext = image.GetExtent()
        width = ext[1]-ext[0]+1
        height = ext[3]-ext[2]+1
        if image.GetNumberOfScalarComponents() == 1:
            imageslice = numpy.flipud(idata.reshape(width,height))
        else:
            imageslice = numpy.flipud(idata.reshape(width,height,image.GetNumberOfScalarComponents()))

        # Now load the first entry from the store
        cs2 = cinema_store.FileStore("./contour.json")
        cs2.load()

        docs = []
        for doc in cs2.find({'theta' : -180, 'phi' : 0, 'contour' : 160}):
            docs.append(doc.data)

        # compare the two
        self.assertTrue(numpy.all(imageslice == docs[0]))

        self.clean_up(cs, "./contour.json")

if __name__ == '__main__':
    unittest.main()
